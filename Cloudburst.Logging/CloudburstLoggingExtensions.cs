﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace Cloudburst.Logging {
	public static class CloudburstLoggingExtensions {
		public static IHostBuilder UseCloudburstLogging(this IHostBuilder builder) {
			builder.ConfigureAppConfiguration((builderContext, config) => {
				IConfigurationRoot currentConfig = config.Build();

				IConfigurationSection serilogConfig = currentConfig.GetSection("Serilog");

				if (!serilogConfig.Exists()) {
					Log.Logger = new LoggerConfiguration()
						.Enrich.FromLogContext()
						.MinimumLevel.Debug()
						.MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
						.MinimumLevel.Override("System", LogEventLevel.Debug)
						.WriteTo.Console(theme: AnsiConsoleTheme.Code)
						.CreateLogger();
					return;
				}

				Log.Logger = new LoggerConfiguration()
					.ReadFrom.Configuration(currentConfig)
					.Enrich.FromLogContext()
					.CreateLogger();
			});

			builder.ConfigureLogging((builderContext, logging) => {
				logging.AddSerilog();
			});

			builder.UseSerilog();

			return builder;
		}
	}
}
